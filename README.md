# Zerrow Startpages 

A collection of start-pages that employ a simple design and web-based art. This collection features the pixel-art of [Zerrow_Z](https://www.instagram.com/zerow_z/). Their pixel art envokes thought, mood and temprature. [Find Zerrow_z's artwork online](https://linktr.ee/Zzerow)

## Getting started

A simple html startpage for Firefox designed for use with with web-based pixelart, gifs and web images. 


![Alt text](https://gitlab.com/locdog/startpages/-/raw/main/Cold-Zerrow/Peek_2021-08-30_12-57.gif)
**Cold Zerrow startpage**

[Link](https://gitlab.com/locdog/startpages/-/raw/main/Hot-Zerrow/Peek_2021-08-30__2.gif)
**Hot Zerrow startpage**

[Link](https://gitlab.com/locdog/startpages/-/raw/main/Term4/Peek_2021-08-30_12-59.gif)
**Term4 startpage**

[Link](https://gitlab.com/locdog/startpages/-/blob/main/Vibe%20Check/Peek_2021-08-30_12-58.gif)
**Vibe Check startpage**

## Features

- Choose greeting based on mood and picture
- Click sub-header to change
- Randomly choose pixel art image
- Click image to randomly change


## Origins

The origional design for these start-pages were developed with the help of [Mariusz Z](https://gitlab.com/vallode). [This guide](https://stpg.tk/guides/basic-startpage/) enabled me to build my first start-page from scratch (almost). u/Epikjustice on reddit contributed the code for switching greetings and images (thank you). The goal is to keep the design simple, useful and artistic.

## Installation
For the startpage: Enable the new tab startpage functionality open Firefox settings and then go to the `home` tab. Under `New Windows and Tabs` select `Custom URLs`  to change your home tab to the GetGoing.html document to serve as the browser start-page. = "file:///home/£YOUR-USER-NAME£/GetGoing.html"

## Editing

### Message and Links
To edit the html document, open it with a text editor. Look in the `body `section of the document, at the bottom, it looks like this...

```
<body>
  <nav>
  <h1>Title</h1>
  <ul>
    <li>Get Going</li>
    <li><a href="https://twitter.com/home">twitter</a><li>
    <li><a href="https://www.youtube.com/">YouTube</a><li>
    <li><a href="https://www.reddit.com/">reddit</a><li>
      </ul> 
 </nav>
```
Change the welcome message by replacing `<h1>Welcome Back Bruv!</h1>` with `<h1>Any message you want</h1>`

Change the links..

```
 <li>Get Going</li>
    <li><a href="https://twitter.com/home">twitter</a><li>
    <li><a href="https://www.youtube.com/">YouTube</a><li>
    <li><a href="https://www.reddit.com/">reddit</a><li>
```
by replacing the links you want. Don't forget to change the title of the link at the end of the line.

### Background Colour and Font 
To change the Background colour look in the `style` section of the document at the top. It looks like this...

```
<head>
  <title>New tab</title>
 <style>
    html {
      align-items: center;
      background-color: #91a3b0 ;
      color: #313131;
      display: flex;
      font: 22px "Courier Prime", Courier, monospace;
      height: 100%;
      justify-content: center;
      margin: 0;
```

Use a [html color code](https://html-color.codes/) to change the background colour.

Use a monospace web font to change the font, [here are a few](https://fonts.google.com/?category=Monospace)


## Support
The origional author of this document is [Mariusz Z](https://gitlab.com/vallode). [His guide](https://stpg.tk/guides/basic-startpage/) got me started making this doc. [The reddit startpage community is also very helpful](https://www.reddit.com/r/startpages/)

## Roadmap
I would like to learn how to add multiple links and clocks and weather reports


## Authors and acknowledgment
A big thank you to [Zerrow_Z](https://linktr.ee/Zzerow) for the web-based art and [Mariusz Z](https://gitlab.com/vallode).

## License
MIT license.

## Project status
Development is slow due to life.

